package com.lvmoney.frame.ai.seetaface.base.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lvmoney.frame.ai.seetaface.base.entity.ComparisonHistory;

/**
 * <p>
 * 比对历史 Mapper 接口
 * </p>
 *
 * @author lvmoney
 * @since 2022-02-10
 */
public interface ComparisonHistoryDao extends BaseMapper<ComparisonHistory> {

}
